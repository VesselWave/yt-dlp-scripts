# VeselWave's YT-DLP Script Repository

Based on https://github.com/jordandalley/yt-dlp-scripts
This is just a repo for some random scripts for downloading things using yt-dlp fork of youtube-dl


# yt-music-album-download.sh

- Downloads entire albums off Youtube Music using yt-dlp: https://github.com/yt-dlp/yt-dlp
- Converts tracks to M4A from the best quality audio feed
- Adds track number, artist, album, title, and release year into id3 tags (removes superfluous information)
- Adds album art as external JPG with high resolution

Depends on

`yt-dlp sacad jq which`

SACAD is used to download album cover https://github.com/desbma/sacad